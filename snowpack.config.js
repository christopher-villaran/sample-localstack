module.exports = {
  mount: {
    build: '/',
    src: '/',
  },
  plugins: [
    [
      '@snowpack/plugin-babel',
      {
        input: ['.js'],
      },
    ],
    [
      '@canarise/snowpack-eslint-plugin',
      {
        globs: ['src/**/*.js'],
        options: {
          /* any eslint options here */
        },
        formatter: 'stylish',
      },
    ],
    '@snowpack/plugin-sass',
  ],
  routes: [
    {
      match: 'all',
      src: '/api/.*',
      dest: (req, res) => proxy.web(req, res),
    },
    {
      match: 'routes',
      src: '.*',
      dest: '/index.html',
    },
  ],
  alias: {
    '@assets': './src/assets',
    '@components': './src/components',
    '@core': './src/core',
    '@hooks': './src/hooks',
    '@pages': './src/pages',
    '@styles': './src/styles',
  },
  optimize: {
    bundle: true,
    minify: true,
    target: 'es2018',
  },
  alias: {
    '@assets': './src/assets',
    '@components': './src/components',
    '@core': './src/core',
    '@hooks': './src/hooks',
    '@pages': './src/pages',
    '@styles': './src/styles',
  },
  buildOptions: {
    baseUrl: 'http://localhost:4566/sample-localstack',
  },
};
