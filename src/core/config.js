export default {
  appId: '13a56e42-6faf-429c-a942-b0e62b4714d5',
  redirectUri: 'http://localhost:8080',
  scopes: ['user.read'],
  authority: 'https://login.microsoftonline.com/swptesting.onmicrosoft.com',
};
