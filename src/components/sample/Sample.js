// Libraries
import joinClasses from 'classnames';
import {
  AuthenticatedTemplate,
  UnauthenticatedTemplate,
  useMsal,
  useAccount,
} from '@azure/msal-react';
import { useEffect, useState } from 'react';

// Styles
import classes from './Sample.module.sass';

const Sample = (props) => {
  const { text, className = '' } = props;
  const { instance, accounts } = useMsal();
  const account = useAccount(accounts[0] || {});
  const [, setAccessToken] = useState();
  const [, setError] = useState(null);

  useEffect(() => {
    async function getToken() {
      if (!account) {
        return;
      }

      const result = await instance.acquireTokenSilent({
        scopes: ['User.Read'],
        account,
      });

      setAccessToken(result.accessToken);
    }

    getToken();
  }, [account, instance]);

  async function login() {
    try {
      await instance.loginPopup();
    } catch (err) {
      setError(err);
    }
  }

  async function logout() {
    try {
      await instance.logout();
    } catch (err) {
      setError(err);
    }
  }

  const styles = {
    sample: joinClasses(classes.sample, className),
  };

  return (
    <div className={styles.sample}>
      <AuthenticatedTemplate>
        <p>Logged In: {text}</p>
        <button type="button" onClick={logout}>
          Logout
        </button>
      </AuthenticatedTemplate>
      <UnauthenticatedTemplate>
        <button type="button" onClick={login}>
          Login
        </button>
      </UnauthenticatedTemplate>
    </div>
  );
};

export default Sample;
