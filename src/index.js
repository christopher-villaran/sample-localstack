// Libraries
import ReactDom from 'react-dom';
import { PublicClientApplication } from '@azure/msal-browser';
import { MsalProvider } from '@azure/msal-react';

// Components
import { Sample } from '@components';

// Misc
import config from '@core/config';

// Styles
import '@styles/base/reset.sass';
import '@styles/base/main.sass';

const publicClientApplication = new PublicClientApplication({
  auth: {
    clientId: config.appId,
    redirectUri: config.redirectUri,
    authority: config.authority,
  },
  cache: {
    cacheLocation: 'sessionStorage',
    storeAuthStateInCookie: true,
  },
});

ReactDom.render(
  <MsalProvider instance={publicClientApplication}>
    <Sample text="This is SAMPLE" />
  </MsalProvider>,
  document.getElementById('root')
);
