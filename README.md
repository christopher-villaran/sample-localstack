* Fresh start
- yarn
- yarn s3-create

* For local development
- yarn dev

* For local deployment
- yarn build





























* Create SQS queue
aws \
sqs create-queue \
--queue-name local-queue \
--endpoint-url http://localhost:4566

* Create SNS topic
aws \
sns create-topic \
--name local-topic \
--endpoint-url http://localhost:4566

* Create Subscription
aws \
sns subscribe \
--notification-endpoint http://localhost:4576/queue/local-queue \
--topic-arn arn:aws:sns:us-east-1:123456789012:local-topic \
--protocol sqs \
--endpoint-url=http://localhost:4566

* Subscription
arn:aws:sns:us-east-1:000000000000:local-topic:5128b1b1-f9ff-423a-9ee8-c64962ee60e6